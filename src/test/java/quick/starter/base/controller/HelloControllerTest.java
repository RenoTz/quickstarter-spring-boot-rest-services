package quick.starter.base.controller;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class HelloControllerTest
{

    @InjectMocks
    private HelloController controller;

    @Test
    public void getHello() throws Exception
    {
        final String hello = this.controller.hello();

        assertEquals("Hello world!", hello);
    }
}
