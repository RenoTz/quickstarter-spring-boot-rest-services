package quick.starter.base.controller.mapper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.util.List;

import org.assertj.core.util.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import quick.starter.base.controller.resource.EmployeResource;
import quick.starter.base.jpa.Employe;

@RunWith(MockitoJUnitRunner.class)
public class EmployeMapperImplTest
{

    @InjectMocks
    private EmployeMapperImpl mapper;

    @Test
    public void testEmployeToEmployeResource()
    {
        // Arrange
        final Employe employe = generateEmploye();
        // Act
        final EmployeResource resource = this.mapper.employeToEmployeResource(employe);
        // Assert
        assertEquals(employe.getId(), resource.getId());
        assertEquals(employe.getNom(), resource.getNom());
        assertEquals(employe.getPrenom(), resource.getPrenom());
    }

    @Test
    public void testEmployeToEmployeResource_nullCase()
    {
        // Act
        final EmployeResource resource = this.mapper.employeToEmployeResource(null);
        // Assert
        assertNull(resource);
    }

    @Test
    public void testListEmployeToListEmployeResource()
    {
        // Arrange
        final Employe employe = generateEmploye();
        // Act
        final List<EmployeResource> resources =
            this.mapper.listEmployeToListEmployeResource(Lists.newArrayList(employe));
        // Assert
        resources.forEach(resource -> {
            assertEquals(employe.getId(), resource.getId());
            assertEquals(employe.getNom(), resource.getNom());
            assertEquals(employe.getPrenom(), resource.getPrenom());
        });
    }

    @Test
    public void testListEmployeToListEmployeResource_casNull()
    {
        // Act
        final List<EmployeResource> resources = this.mapper.listEmployeToListEmployeResource(null);
        // Assert
        assertNull(resources);
    }

    private static Employe generateEmploye()
    {
        final Employe employe = new Employe();
        employe.setId(123L);
        employe.setNom("nom");
        employe.setPrenom("prenom");

        return employe;
    }

}
